const fs = require('fs');
const Jimp = require("jimp");
const Promise = require('bluebird');
const prompt = require('prompt');

const schema = {
    properties: {
        directory: {
            name: 'Directory',
            description: 'Directory images are located',
            required: true
        },
        extension: {
            description: 'Extension of images to read (jpg)',
        },
        newExtension: {
            description: 'The extension to convert images (png)',
        }
    }
};

prompt.start();

prompt.get(schema, function(err, result) {
    let extension = result.extension || 'jpg';
    let newExtension = result.newExtension || 'png';
    let directory = result.directory;

    if(directory[directory.length - 1] !== '/') {
        directory += '/';
    }

    if(extension[0] !== '.') {
        extension = '.' + extension;
    }

    if(newExtension[0] !== '.') {
        newExtension = '.' + newExtension;
    }

    const files = fs.readdirSync(directory).filter(function(file) { return file.match(new RegExp(`.*(${extension})`, 'ig')); });
    console.log(`Scanning directory ${directory} for ${extension} files`);

    const processFile = function(file) {
        const fileName = file.split('.')[0];
        const newFileName = fileName + newExtension;
        console.log("Reading file: ", directory + file);
        return Jimp.read(directory + file)
            .then(function(image) {
                return Promise.all([
                    image.clone()
                        .scale(1.5)
                        .write(directory + '1.5x/' + newFileName),
                    image.clone()
                        .scale(2)
                        .write(directory + '2x/' + newFileName),
                    image.clone()
                        .scale(3)
                        .write(directory + '3x/' + newFileName)
                ])
                    .then(function() {
                        console.log("Created file: ", newFileName);
                    });
            })
            .catch(function(error) {
                console.error("Error occurred: ", error.message);
            });
    };

    const processFiles = function(files) {
        if(files.length) {
            processFile(files.shift())
                .then(function () {
                    processFiles(files);
                });
        } else {
            console.log("Completed processing images.");
        }
    };

    processFiles(files);
});